package id.branditya.binarchapter4tugas2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import id.branditya.binarchapter4tugas2.databinding.FragmentDialogBinding

class FirstDialogFragment : DialogFragment() {
    private var _binding : FragmentDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDialogBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnFragmentOne.setOnClickListener {
            Toast.makeText(requireContext(),"Tombol dialog fragment ditekan",Toast.LENGTH_SHORT).show()
        }
        binding.btnFragmentTwo.setOnClickListener {
            dialog?.dismiss()
        }
    }
}