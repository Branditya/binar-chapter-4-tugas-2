package id.branditya.binarchapter4tugas2

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import id.branditya.binarchapter4tugas2.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {
    private var _binding : FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showAlertDialogStandard()
        showAlertDialogAction()
        showAlertDialogCustom()
        showDialogFragment()
    }

    private fun showAlertDialogStandard() {
        binding.btnDialogStandard.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("DIALOG STANDARD")
            dialog.setMessage("Ini dialog standard tanpa action")
            dialog.show()
        }
    }

    private fun showAlertDialogAction() {
        binding.btnDialogAction.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("DIALOG ACTION")
            dialog.setMessage("Ini dialog Action")
            dialog.setNeutralButton("Netral"){ _,_ ->
                createToast("Action Netral").show()
            }
            dialog.setPositiveButton("Positif"){ _,_ ->
                createToast("Action Positif").show()
            }
            dialog.setNegativeButton("Negatif"){ _,_ ->
                createToast("Action Negatif").show()
            }
            dialog.setCancelable(false)
            dialog.show()
        }
    }

    private fun showAlertDialogCustom() {
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.alert_dialog_custom, null, false)
        val title = customLayout.findViewById<TextView>(R.id.tv_title)
        val buttonCustom = customLayout.findViewById<Button>(R.id.btn_one)

        title.text = "DIALOG CUSTOM"
        buttonCustom.text = "Action"

        val builder = AlertDialog.Builder(requireContext())
        builder.setView(customLayout)
        val dialog = builder.create()
        buttonCustom.setOnClickListener {
            createToast("Button Dialog Custom").show()
            dialog.dismiss()
        }

        binding.btnDialogCustom.setOnClickListener {
            dialog.show()
        }
    }

    private fun showDialogFragment() {
        val dialogFragment = FirstDialogFragment()
        binding.btnDialogFragment.setOnClickListener {
            dialogFragment.show(childFragmentManager,null)
        }
    }

    private fun createToast(message : String):Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
    }
}